import React, { Fragment, useEffect } from 'react'
import { useSelector } from 'react-redux';
import style from './CheckOut.module.css'
import { useDispatch } from 'react-redux';
import { datVeAction, LayChiTietPhongVeAction } from '../../redux/actions/QuanLyDatVeAction';
import { layThongTinNguoiDungAction } from '../../redux/actions/QuanLyNguoiDungAction'
import './CheckOut.css'
import { DAT_GHE } from './../../redux/types/QuanLyDatVeType'
import _ from 'lodash'
import { ThongTinDatVe } from '../../_core/modules/ThongTinDatVe';
import { Tabs } from 'antd'
import { UserOutlined, HomeOutlined } from '@ant-design/icons';
import moment from 'moment';
import { NavLink } from 'react-router-dom';
import { history } from '../../App'
import { TOKEN, USER_LOGIN } from '../../utils/settings/config'

function CheckOut(props) {

    const { userLogin } = useSelector(state => state.QuanLyNguoiDungReducer);

    const { chiTietPhongVe, danhSachGheDangDat } = useSelector(state => state.QuanLyDatVeReducer);


    const dispatch = useDispatch();

    useEffect(() => {
        const action = LayChiTietPhongVeAction(props.match.params.id);
        dispatch(action)
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    const { thongTinPhim, danhSachGhe } = chiTietPhongVe;

    const renderGhe = danhSachGhe.map((item, index) => {

        let classGheVip = item.loaiGhe === 'Vip' ? 'gheVip' : '';
        let classGheDaDat = item.daDat === true ? 'gheDaDat' : '';

        let indexGheDangDat = danhSachGheDangDat.findIndex(gheDD => gheDD.maGhe === item.maGhe);

        let classGheDaDuocDat = '';
        if (userLogin.taiKhoan === item.taiKhoanNguoiDat) {
            classGheDaDuocDat = 'gheDaDuocDat';
        }

        if (indexGheDangDat !== -1) {
            classGheDaDat = 'gheDangDat'
        }

        return <Fragment key={index}>
            <button disabled={item.daDat} onClick={() => {
                dispatch({
                    type: DAT_GHE,
                    datGhe: item
                })
            }} type='button' className={`${classGheVip} ${classGheDaDat} ${indexGheDangDat} ${classGheDaDuocDat} border border-green-900 text-center w-12 m-1 h-10 rounded-md text-yellow-500 font-bold hover:bg-green-900 hover:text-white`}>
                {classGheDaDuocDat !== '' ? <UserOutlined style={{ color: 'red', fontSize: '20px' }} /> : item.tenGhe}
            </button>

        </Fragment>
    })

    return (
        <div className='px-36 m-h-screen'>
            <h1 className='text-center text-4xl font-bold text-yellow-500'>{thongTinPhim.tenRap}</h1>
            <div className='grid grid-cols-12'>
                <div className='col-span-9 '>
                    <div className='flex flex-col items-center'>
                        <div className='h-4 w-4/5 bg-green-900 flex justify-center'>
                        </div>
                        <div className={`${style['hinh_thang']} w-4/5 flex justify-center font-bold text-lg`}>Màn Hình</div>
                    </div>
                    <div className='py-10 px-20 text-center'>
                        {renderGhe}
                    </div>
                    <div className='grid grid-cols-5'>
                        <p className='flex flex-col items-center'>
                            <button type='button' className='cursor-not-allowed border border-green-900 text-center w-12 m-1 h-10 rounded-md text-yellow-500 font-bold'>01</button>
                            <span className='py-3 text-green-900 font-bold text-lg'>Ghế Thường</span>
                        </p>
                        <p className='flex flex-col items-center'>
                            <button type='button' className='cursor-not-allowed border-2 border-red-500 text-center w-12 m-1 h-10 rounded-md text-green-900 font-bold'>01</button>
                            <span className='py-3 text-red-500 font-bold text-lg'>Ghế Vip</span>
                        </p>
                        <p className='flex flex-col items-center'>
                            <button type='button' className='cursor-not-allowed border border-green-900 text-center w-12 m-1 h-10 rounded-md text-white bg-green-900 font-bold'>01</button>
                            <span className='py-3 text-green-900 font-bold text-lg'>Ghế Do Mình Chọn</span>
                        </p>
                        <p className='flex flex-col items-center'>
                            <button type='button' className='cursor-not-allowed border border-green-900 text-center w-12 m-1 h-10 rounded-md text-white bg-yellow-500 font-bold'>01</button>
                            <span className='py-3 text-yellow-500 font-bold text-lg'>Ghế Người Khác Đặt</span>
                        </p>
                        <p className='flex flex-col items-center'>
                            <button type='button' className='cursor-not-allowed border border-green-900 text-center w-12 m-1 h-10 rounded-md font-bold' style={{ backgroundColor: '#9ff79f' }}><UserOutlined style={{ color: 'red', fontSize: '20px' }} /></button>
                            <span className='py-3 text-green-400 font-bold text-lg'>Ghế Do Mình Đặt</span>
                        </p>
                    </div>
                </div>
                <div className='col-span-3 border-l px-4 shadow-lg shadow-indigo-500/40'>
                    <h3 className='text-center text-4xl font-bold text-green-900 mb-4'>
                        {danhSachGheDangDat.reduce((tongTien, ghe, index) => {
                            return tongTien += ghe.giaVe;
                        }, 0).toLocaleString()}
                        đ</h3>
                    <hr />
                    <h3 className='text-2xl pt-2 text-green-900'>{thongTinPhim.tenPhim}</h3>
                    <p className='mb-0 text-green-900'>Địa chỉ: <span className='text-lg text-yellow-500'>{thongTinPhim.diaChi}</span></p>

                    <p className='text-green-900 my-0'>Ngày chiếu: <span className='text-lg text-yellow-500'>{thongTinPhim.ngayChieu}</span></p>
                    <p className='text-green-900'>Giờ chiếu: <span className='text-lg text-yellow-500'>{thongTinPhim.gioChieu}</span></p>

                    <hr />
                    <div className='grid grid-cols-8 py-3'>
                        <div className='col-1'>Ghế:</div>
                        <div className='col-6 col-span-4 text-lg text-green-900 w-full flex flex-wrap grid grid-cols-4'>
                            {_.sortBy(danhSachGheDangDat, ['tenGhe']).map((ghe, index) => {
                                return <span className='mx-1 text-yellow-500 font-bold text-center' key={index}>{ghe.tenGhe}</span>
                            })}
                        </div>
                        <div className='col-3 col-span-3 flex justify-end text-xl font-bold text-green-900 w-full'>
                            {danhSachGheDangDat.reduce((tongTien, ghe, index) => {
                                return tongTien += ghe.giaVe;
                            }, 0).toLocaleString()}
                            đ
                        </div>
                    </div>
                    <hr />
                    <div className='py-3 flex flex-col'>
                        <span className='text-base text-green-900'>Email:</span>
                        <span className='text-xl text-yellow-500'>{userLogin.email}</span>
                    </div>
                    <hr />
                    <div className='py-3 flex flex-col'>
                        <span className='text-base text-green-900'>Phone:</span>
                        <span className='text-xl text-yellow-500'>{userLogin.soDT}</span>
                    </div>
                    <hr />
                    <div className='h-1/3 flex flex-col justify-end items-center'>
                        <button onClick={() => {
                            const thongTinDatVe = new ThongTinDatVe();
                            thongTinDatVe.maLichChieu = props.match.params.id;
                            thongTinDatVe.danhSachVe = danhSachGheDangDat;
                            dispatch(datVeAction(thongTinDatVe));


                        }} type="button" className="border h-12 w-3/4 font-bold text-base border-green-900 text-yellow-600 rounded-md px-4 py-2 m-2 transition duration-500 ease select-none bg-white hover:bg-green-900 hover:text-white focus:outline-none focus:shadow-outline">
                            Đặt Vé
                        </button>
                    </div>
                </div>
            </div>
        </div >
    )
}



const { TabPane } = Tabs;


// eslint-disable-next-line import/no-anonymous-default-export
export default function (props) {

    const { userLogin } = useSelector(state => state.QuanLyNguoiDungReducer)

    const operations = <Fragment>
        {!_.isEmpty(userLogin) ? <div className='flex'>
            <NavLink to='/profile' className='flex'>
                <img className='w-14 h-14 rounded-full' src='https://c4.wallpaperflare.com/wallpaper/240/34/575/8k-vegetto-4k-super-saiyan-blue-wallpaper-preview.jpg' alt={userLogin.taiKhoan} />
                <span className='flex items-center text-lg font-bold mx-2 cursor-pointer text-yellow-500'>{userLogin.taiKhoan}</span>
            </NavLink>
            <button onClick={() => {
                sessionStorage.removeItem(USER_LOGIN);
                sessionStorage.removeItem(TOKEN);
                history.push('/home');
                window.location.reload();
            }} className="self-center border-l-2 border-green-900 px-8 py-3 text-yellow-500 text-lg font-bold hover:text-green-900">Đăng xuất</button>
        </div> : ''}
    </Fragment>

    return <div className='px-24 py-4'>
        <Tabs tabBarExtraContent={operations} defaultActiveKey='1'>
            <TabPane tab='01 CHỌN GHẾ & THANH TOÁN' key='1'>
                <CheckOut {...props} />
            </TabPane>
            <TabPane tab='02 KẾT QUẢ ĐẶT VÉ' key='2'>
                <KetQuaDatVe {...props} />
            </TabPane>
            <TabPane tab={<button onClick={() => {
                history.push('/home')
            }} type='button' className='text-yellow-500 font-bold hover:text-green-900'><HomeOutlined style={{ fontSize: '36px' }} /></button>} key='3' />
        </Tabs>
    </div>
}



function KetQuaDatVe(props) {

    const dispatch = useDispatch()

    const { thongTinNguoiDung } = useSelector(state => state.QuanLyNguoiDungReducer)

    useEffect(() => {
        dispatch(layThongTinNguoiDungAction())
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    const renderHistoryTicket = () => {
        return thongTinNguoiDung.thongTinDatVe.map((item, index) => {
            return <div key={index} className="p-2 lg:w-1/3 md:w-1/2 w-full">
                <div className="h-full flex items-center border-gray-200 border p-4 rounded-lg">
                    <img src={item.hinhAnh} alt={item.tenPhim} className="w-16 h-20 bg-gray-100 object-cover object-center flex-shrink-0 rounded mr-4" />
                    <div className="flex-grow">
                        <h2 className="text-green-900 mb-0">{item.danhSachGhe[0].tenHeThongRap}</h2>
                        <span className="text-green-900">{item.danhSachGhe[0].tenRap}</span>
                        <h2 className="text-yellow-500 title-font font-medium">{item.tenPhim}</h2>
                        <div className='flex flex-col'>
                            <span className="text-green-900">Giá vé: <span className='text-yellow-500'>{item.giaVe}</span> đ</span>
                            <span className="text-green-900">Thời lượng: <span className='text-yellow-500'>{item.thoiLuongPhim}</span> phút</span>
                            <span className='text-green-900'>Ngày đặt: <span className='text-yellow-500'>{moment(item.ngayDat).format('DD/MM/YYYY')}</span> <span className='text-yellow-500'>{moment(item.ngayDat).format('hh:mm A')}</span></span>
                        </div>
                        <div className='flex flex-wrap'>Ghế: {item.danhSachGhe.map((dsGhe, index) => {
                            return <div key={index}>
                                <span className='px-1 text-yellow-500'>{dsGhe.tenGhe}</span>
                            </div>
                        })}</div>

                    </div>
                </div>
            </div>
        })
    }

    return <section className="text-gray-600 body-font">
        <div className="container px-5 py-8 mx-auto">
            <div className="flex flex-col text-center w-full mb-20">
                <h1 className="sm:text-3xl text-2xl font-bold title-font mb-4 text-green-900">Lịch sử đặt vé</h1>
            </div>
            <div className="flex flex-wrap -m-2">

                {renderHistoryTicket()}

            </div>
        </div>
    </section>

}