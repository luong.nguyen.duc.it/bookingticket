import { combineReducers, applyMiddleware, createStore } from 'redux';
import thunk from 'redux-thunk'
import { QuanLyCarouselReducer } from './reducers/QuanLyCarouselReducer';
import { QuanLyFilmReducer } from './reducers/QuanLyFilmReducer';
import { QuanLyRapReducer } from './reducers/QuanLyRapReducer';
import { QuanLyNguoiDungReducer } from './reducers/QuanLyNguoiDungReducer';
import { QuanLyDatVeReducer } from './reducers/QuanLyDatVeReducer';
import { LoadingReducer } from './reducers/LoadingReducer';

const rootReducer = combineReducers({
    QuanLyCarouselReducer,
    QuanLyFilmReducer,
    QuanLyRapReducer,
    QuanLyNguoiDungReducer,
    QuanLyDatVeReducer,
    LoadingReducer
});


export const store = createStore(rootReducer, applyMiddleware(thunk));