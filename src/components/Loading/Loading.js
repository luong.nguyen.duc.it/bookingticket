import React, { Fragment } from 'react'
import { useDispatch, useSelector } from 'react-redux';

export default function Loading(props) {
    const dispatch = useDispatch();

    const { isLoading } = useSelector(state => state.LoadingReducer);


    return (
        <Fragment>
            {isLoading ? <div className='fixed w-full h-full bg-gray-200 flex justify-center items-center z-50 opacity-90'>
                <img className='h-24 w-24' src='https://i.pinimg.com/originals/ce/a7/c1/cea7c1c3377295b4e3ba605488ea3741.png' alt='PHOENIX' />
            </div> : ''}
        </Fragment>

    )
}
