import React, { Fragment, useState } from 'react'
import {
    DatePicker,
    Input,
    InputNumber,
    Switch,
} from 'antd';
import { useFormik } from 'formik'
import * as Yup from "yup";
import { useDispatch } from 'react-redux';
import moment from 'moment';
import { addFilmAction } from '../../../../redux/actions/QuanLyFilmACtion';
import { GROUP } from '../../../../utils/settings/config';

export default function AddFilm(props) {
    const { TextArea } = Input;

    const [img, setImg] = useState('');

    const dispatch = useDispatch();
    const formik = useFormik({
        initialValues: {
            tenPhim: '',
            trailer: '',
            moTa: '',
            ngayKhoiChieu: '',
            dangChieu: false,
            sapChieu: false,
            hot: false,
            danhGia: 0,
            hinhAnh: {},
        },
        // validationSchema: Yup.object({
        //     taiKhoan: Yup.string()
        //         .min(2, "Too Short!")
        //         .max(50, "Too Long!")
        //         .required("Account is required"),
        //     matKhau: Yup.string()
        //         .min(2, "Too Short!")
        //         .max(50, "Too Long!")
        //         .required("Password is required"),

        // }),
        onSubmit: values => {
            values.maNhom = GROUP;
            //tạo đối tượng formdata => đưa values từ formik vào formdata
            let dataFilm = new FormData();
            for (let key in values) {
                if (key !== 'hinhAnh') {
                    dataFilm.append(key, values[key]);
                }
                else {
                    dataFilm.append('File', values.hinhAnh, values.hinhAnh.name);
                }
            }

            dispatch(addFilmAction(dataFilm));
        },
    });

    const handleChangeDatePicker = (value) => {
        let ngayKhoiChieu = moment(value).format('DD/MM/YYYY');
        formik.setFieldValue('ngayKhoiChieu', ngayKhoiChieu)
    }
    const handleChangeSwitch = (name) => {
        return (value) => { formik.setFieldValue(name, value) }
    }

    const handleChangeFile = (e) => {

        let file = e.target.files[0]

        if (file.type === 'image/jpeg' || file.type === 'image/jpeg' || file.type === 'image/jpg' || file.type === 'image/png') {
            let reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = (e) => {
                setImg(e.target.result) //Hinh base 64
            }
            formik.setFieldValue('hinhAnh', file)
        }

    }

    return (
        <Fragment>
            <h2 className='text-4xl font-bold text-center text-yellow-500'>Thêm mới Film</h2>
            <form onSubmit={formik.handleSubmit}>
                <div className='grid grid-cols-12 my-4'>
                    <span className='col-start-4'>Tên Phim:</span>
                    <Input className='col-span-4' name='tenPhim' onChange={formik.handleChange} />
                </div>
                <div className='grid grid-cols-12 my-4'>
                    <span className='col-start-4'>Trailer:</span>
                    <Input className='col-span-4' name='trailer' onChange={formik.handleChange} />
                </div>
                <div className='grid grid-cols-12 my-4'>
                    <span className='col-start-4'>Mô tả:</span>
                    <TextArea className='col-span-4' name='moTa' rows={4} onChange={formik.handleChange} />
                </div>
                <div className='grid grid-cols-12 my-4'>
                    <span className='col-start-4'>Ngày khởi chiếu:</span>
                    <DatePicker format={'DD/MM/YYYY'} onChange={handleChangeDatePicker} />
                </div>
                <div className='grid grid-cols-12 my-4'>
                    <span className='col-start-4'>Đang chiếu:</span>
                    <Switch className='w-1/3' onChange={handleChangeSwitch('dangChieu')} />
                </div>
                <div className='grid grid-cols-12 my-4'>
                    <span className='col-start-4'>Sắp chiếu:</span>
                    <Switch className='w-1/3' onChange={handleChangeSwitch('sapChieu')} />
                </div>
                <div className='grid grid-cols-12 my-4'>
                    <span className='col-start-4'>Hot:</span>
                    <Switch className='w-1/3' onChange={handleChangeSwitch('hot')} />
                </div>
                <div className='grid grid-cols-12 my-4'>
                    <span className='col-start-4'>Đánh giá:</span>
                    <InputNumber onChange={(value) => { formik.setFieldValue('danhGia', value) }} min={1} max={10} />
                </div>
                <div className='grid grid-cols-12 my-4'>
                    <span className='col-start-4'>Hình ảnh:</span>
                    <input className='col-span-2' type='file' onChange={handleChangeFile} accept='image/jpeg, image/jpg, image/png' />
                </div>
                <div className='grid grid-cols-12 my-4'>
                    <img className='col-start-5 w-36 h-36 rounded-md' src={img} alt='...' />
                </div>


                <div className='text-center mt-24'>
                    <button type='submit' className="w-36 h-12 text-yellow-500 text-xl font-bold border-2 border-green-900 rounded hover:bg-green-900 hover:text-white">Thêm</button>
                </div>
            </form >
        </Fragment >
    )
}
