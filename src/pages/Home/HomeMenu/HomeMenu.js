import React, { useState, Fragment } from 'react'
import { Tabs } from 'antd';
import moment from 'moment'
import { history } from '../../../App';
const { TabPane } = Tabs;

export default function HomeMenu(props) {

    const [tabPosition] = useState('left');



    const renderHeThongRap = () => {
        //lay he thong rap
        return props.lstRapChieu?.map((item, index) => {
            return <TabPane key={index} tab={<img src={item.logo} alt={item.tenHeThongRap} className='rounded-full h-16 w-16' />}>
                {/* lay thong tin cum rap */}
                <Tabs tabPosition={tabPosition}>
                    {item.lstCumRap?.slice(0, 5).map((cumRap, index) => {
                        return <TabPane key={index} tab={
                            <div className='w-96 flex '>
                                <img src={cumRap.hinhAnh} alt={cumRap.tenCumRap} className='h-16 w-16 mr-4' />
                                <div >
                                    <div className='text-green-900 font-bold text-lg text-start'>
                                        {cumRap.tenCumRap}
                                    </div>
                                    <div className='text-red-500 text-start'>
                                        {cumRap.diaChi}
                                    </div>
                                    <p className='text-yellow-600 text-start'>Chi tiết</p>
                                </div>
                            </div>
                        }>


                            {/* lay danh sach film */}
                            {cumRap.danhSachPhim?.slice(0, 5).map((film, index) => {
                                return <Fragment key={index} >

                                    <div className='flex my-5'>
                                        <div className='h-24 w-24 mr-4'>
                                            <img src={film.hinhAnh} alt={film.tenPhim} className='h-full w-full' />
                                        </div>
                                        <div >
                                            <div className='text-green-900 font-bold text-lg'>
                                                {film.tenPhim}
                                            </div>
                                            <div className='grid grid-cols-5 gap-1'>
                                                {/* lay thong tin gio chieu */}
                                                {film.lstLichChieuTheoPhim?.slice(0, 9).map((lichChieu, index) => {
                                                    return <button onClick={() => {
                                                        history.push(`checkout/${lichChieu.maLichChieu}`)
                                                    }} key={index} type="button" className="border h-12 font-bold text-base border-green-900 text-yellow-600 rounded-md px-4 py-2 m-2 transition duration-500 ease select-none hover:bg-green-900 hover:text-white focus:outline-none focus:shadow-outline">
                                                        {moment(lichChieu.ngayChieuGioChieu).format('hh:mm A')}
                                                    </button>
                                                })}
                                            </div>

                                        </div>
                                    </div>
                                    <hr />

                                </Fragment>
                            })}


                        </TabPane>
                    })}
                </Tabs>


            </TabPane>
        })
    }


    return (
        <>
            <Tabs
                tabPosition={tabPosition}>

                {renderHeThongRap()}
            </Tabs>
        </>

    )
}
