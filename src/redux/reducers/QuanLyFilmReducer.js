import { SET_FILM_DANG_CHIEU, SET_LIST_FILM, SET_FILM_SAP_CHIEU, SET_DETAIL_FILM, SET_EDIT_FILM } from "../types/FilmType"
import _ from 'lodash'
const initialState = {
    lstFilm: [],
    dangChieu: true,
    sapChieu: true,
    lstFilmDefault: [],
    filmDetail: {},

    filmEdit: {}
}

// eslint-disable-next-line import/no-anonymous-default-export
export const QuanLyFilmReducer = (state = initialState, action) => {
    switch (action.type) {

        case SET_LIST_FILM: {
            state.lstFilm = action.lstFilm;
            state.lstFilmDefault = action.lstFilm;
            return { ...state }
        }
        case SET_FILM_DANG_CHIEU: {
            // state.dangChieu = !state.dangChieu;
            state.lstFilm = _.filter(state.lstFilmDefault, { dangChieu: true });
            return { ...state }
        }
        case SET_FILM_SAP_CHIEU: {
            // state.sapChieu = !state.sapChieu;
            state.lstFilm = _.filter(state.lstFilmDefault, { sapChieu: true });
            return { ...state }
        }
        case SET_DETAIL_FILM: {
            state.filmDetail = action.filmDetail;
            return { ...state }
        }
        case SET_EDIT_FILM: {
            state.filmEdit = action.data;
            return { ...state }
        }
        default:
            return state
    }
}
