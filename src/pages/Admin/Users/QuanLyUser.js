import React, { Fragment, useEffect } from 'react'
import { Table } from 'antd';
import { EditOutlined, DeleteOutlined, ScheduleOutlined } from '@ant-design/icons';
import { history } from '../../../App';
import { useDispatch, useSelector } from 'react-redux';
import { Input } from 'antd';
import { dellFilmAction } from '../../../redux/actions/QuanLyFilmACtion'
import _ from 'lodash';
import { getListUserAction } from '../../../redux/actions/QuanLyNguoiDungAction';

const { Search } = Input;
const onSearch = (value) => console.log(value);
export default function QuanLyUser() {
    const dispatch = useDispatch();

    const { listUsers } = useSelector(state => state.QuanLyNguoiDungReducer)

    console.log({ listUsers })

    useEffect(() => {
        // eslint-disable-next-line react-hooks/exhaustive-deps
        dispatch(getListUserAction())
    }, [])

    const columns = [
        {
            title: 'Email',
            dataIndex: 'email',
        },
        {
            title: 'Tài Khoản',
            dataIndex: 'taiKhoan',
        },
        {
            title: 'Họ Tên',
            dataIndex: 'hoTen',

        },
        {
            title: 'Loại Người Dùng',
            dataIndex: 'maLoaiNguoiDung',
            render: (text, user) => {
                return <Fragment>
                    {user.maLoaiNguoiDung === 'QuanTri' ? <span className='text-red-500'>{user.maLoaiNguoiDung}</span> : <span className='text-green-500'>{user.maLoaiNguoiDung}</span>}
                </Fragment>
            }
        },
        {
            title: 'Số Điện Thoại',
            dataIndex: 'soDt',

        },
        {
            title: '',
            dataIndex: 'id',
            render: (text, record) => {
                return <div className='flex'>
                    <button className='mx-4 text-green-500' title='Sửa' onClick={() => {
                        history.push(`/admin/films/edit/${record.maPhim}`)
                    }}>
                        <EditOutlined style={{ fontSize: 25 }} />
                    </button>
                    <button className='mx-4 text-red-500' title='Xóa' onClick={() => {
                        dispatch(dellFilmAction(record.maPhim))
                    }}>
                        <DeleteOutlined style={{ fontSize: 25 }} />
                    </button>
                </div>
            },
        },
    ];

    return (
        <div className='container mt-4'>
            <h2 className='text-4xl font-bold text-center text-yellow-500'>Quản lý Users</h2>
            <div className='mt-10 flex justify-between'>
                <button type='button' className='border-2 border-green-900 rounded w-24 h-10 text-lg font-bold text-yellow-500 hover:text-white hover:bg-green-900' onClick={() => {
                    history.push('/admin/films/add')
                }}>Thêm </button>
                <div className='w-1/2'>
                    <Search size='large' placeholder="Bạn muốn tìm gì?..." onSearch={onSearch} enterButton />
                </div>
            </div>
            <Table columns={columns} dataSource={listUsers} />
        </div>
    )
}
