import React, { Fragment } from 'react'
import { NavLink } from 'react-router-dom'
import { useSelector } from 'react-redux';
import _ from 'lodash';
import { TOKEN, USER_LOGIN } from '../../../../utils/settings/config';
import { history } from './../../../../App';
export default function Header(props) {

    const { userLogin } = useSelector(state => state.QuanLyNguoiDungReducer);

    const renderLogin = () => {
        if (_.isEmpty(userLogin)) {
            return <Fragment>
                <NavLink to='/login'>
                    <button className="self-center border-2 border-yellow-500 px-8 py-3 rounded text-yellow-500 text-lg font-bold hover:text-white hover:bg-yellow-500">Đăng nhập</button>
                </NavLink>
                <NavLink to='/register'>
                    <button className="self-center border-2 border-green-900 px-8 py-3 font-semibold rounded text-yellow-500 text-lg font-bold hover:text-white hover:bg-green-900">Đăng ký</button>
                </NavLink>
            </Fragment>
        }

        return <Fragment>
            <NavLink to='/profile' className='flex'>
                <img className='w-14 h-14 rounded-full' src='https://c4.wallpaperflare.com/wallpaper/240/34/575/8k-vegetto-4k-super-saiyan-blue-wallpaper-preview.jpg' alt={userLogin.taiKhoan} />
                <span className='flex items-center text-lg font-bold mx-2 cursor-pointer text-yellow-500'>{userLogin.taiKhoan}</span>
            </NavLink>

            <button onClick={() => {
                sessionStorage.removeItem(USER_LOGIN);
                sessionStorage.removeItem(TOKEN);
                history.push('/home');
                window.location.reload();
            }} className="self-center border-l-2 border-green-900 px-8 py-3 text-yellow-500 text-lg font-bold hover:text-white">Đăng xuất</button>
        </Fragment>
    }

    return (
        <header className="p-4 bg-coolGray-800 text-coolGray-100 bg-black bg-opacity-40 fixed w-full z-10 text-white">
            <div className="container flex justify-between h-16 mx-auto">
                <NavLink to="/" aria-label="Back to homepage" className="flex items-center p-2">
                    <img src='https://i.pinimg.com/originals/ce/a7/c1/cea7c1c3377295b4e3ba605488ea3741.png' alt='Phonix' className='w-14 h-14 cursor-pointer' />
                </NavLink>
                <ul className="items-stretch hidden space-x-3 lg:flex">
                    <li className="flex">
                        <NavLink to='/home' className="flex items-center px-4 -mb-1 text-2xl text-yellow-500 font-bold hover:text-green-900" activeClassName='border-b-4 border-green-900' >Trang chủ</NavLink>
                    </li>
                    <li className="flex">
                        <NavLink to="/contact" className="flex items-center px-4 -mb-1 isActive text-2xl text-yellow-500 font-bold hover:text-green-900" activeClassName='border-b-4 border-green-900'>Liên hệ</NavLink>
                    </li>
                    <li className="flex">
                        <NavLink to="/news" className="flex items-center px-4 -mb-1 text-2xl text-yellow-500 font-bold hover:text-green-900" activeClassName='border-b-4 border-green-900'>Tin tức</NavLink>
                    </li>
                </ul>
                <div className="grid grid-cols-2 gap-2 items-center alig flex-shrink-0 hidden lg:flex">

                    {renderLogin()}


                </div>
                <button className="p-4 lg:hidden">
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" className="w-6 h-6 text-gray-100">
                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M4 6h16M4 12h16M4 18h16" />
                    </svg>
                </button>
            </div>
        </header>


    )
}
