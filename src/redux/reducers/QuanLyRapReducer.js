import { SET_LIST_RAP } from './../types/RapType';

const initialState = {
    lstRapChieu: []
}

// eslint-disable-next-line import/no-anonymous-default-export
export const QuanLyRapReducer = (state = initialState, action) => {
    switch (action.type) {

        case SET_LIST_RAP: {
            state.lstRapChieu = action.lstRapChieu;
            return { ...state };
        }

        default:
            return state
    }
}
