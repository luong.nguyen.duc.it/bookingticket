import { SET_DAT_VE, DAT_GHE, DAT_VE_HOAN_TAT } from './../types/QuanLyDatVeType';
import { ThongTinLichChieu } from '../../_core/modules/ThongTinPhongVe'

const initialState = {
    chiTietPhongVe: new ThongTinLichChieu(),
    danhSachGheDangDat: []
}

// eslint-disable-next-line import/no-anonymous-default-export
export const QuanLyDatVeReducer = (state = initialState, action) => {
    switch (action.type) {

        case SET_DAT_VE: {
            state.chiTietPhongVe = action.chiTiet;
            return { ...state }
        }
        case DAT_GHE: {
            let danhSachGheCapNhat = [...state.danhSachGheDangDat];
            let index = danhSachGheCapNhat.findIndex(gheDD => gheDD.maGhe === action.datGhe.maGhe);
            if (index !== -1) {
                // nếu tìm thấy ghế được trọn trong mảng có nghĩa là trước đó ghế đã được click vào mảng rồi => xóa đi.
                danhSachGheCapNhat.splice(index, 1)

            }
            else {
                danhSachGheCapNhat.push(action.datGhe);
            }
            return { ...state, danhSachGheDangDat: danhSachGheCapNhat }
        }

        case DAT_VE_HOAN_TAT: {
            state.danhSachGheDangDat = [];
            return { ...state }
        }

        default:
            return state
    }
}
