import {
    DesktopOutlined,
    FileOutlined,
    UserOutlined,
} from '@ant-design/icons';
import { Breadcrumb, Layout, Menu, message } from 'antd';
import React, { Fragment, useState, useEffect } from 'react';
import './AdminTemplate.css'
import { Route } from 'react-router';
import { USER_LOGIN, TOKEN } from '../../utils/settings/config';
import { Redirect, NavLink } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { history } from '../../App';
import _ from 'lodash'


const { Header, Content, Footer, Sider } = Layout;



export const AdminTemplate = (props) => {
    const { Component, ...restRoute } = props;
    const [collapsed, setCollapsed] = useState(false);

    useEffect(() => {
        window.scrollTo(0, 0);
    })

    const { userLogin } = useSelector(state => state.QuanLyNguoiDungReducer);

    if (!sessionStorage.getItem(USER_LOGIN)) {
        message.error('Bạn chưa đăng nhập!')
        return <Redirect to='/login' />
    }
    if (userLogin.maLoaiNguoiDung !== 'QuanTri') {
        message.error('Bạn không có quyền truy cập vào trang này!')
        return <Redirect to='/home' />
    }


    const operations = <Fragment>
        {!_.isEmpty(userLogin) ? <div className='flex'>
            <NavLink to='/profile' className='flex'>
                <img className='w-14 h-14 rounded-full' src='https://c4.wallpaperflare.com/wallpaper/240/34/575/8k-vegetto-4k-super-saiyan-blue-wallpaper-preview.jpg' alt={userLogin.taiKhoan} />
                <span className='flex items-center text-lg font-bold mx-2 cursor-pointer text-yellow-500'>{userLogin.taiKhoan}</span>
            </NavLink>
            <button onClick={() => {
                sessionStorage.removeItem(USER_LOGIN);
                sessionStorage.removeItem(TOKEN);
                history.push('/home');
                window.location.reload();
            }} className="self-center border-l-2 border-green-900 px-8 py-3 text-yellow-500 text-lg font-bold hover:text-green-900">Đăng xuất</button>
        </div> : ''}
    </Fragment>


    return <Route {...restRoute} render={(propsRoute) => {
        return <Fragment>
            <Layout style={{ minHeight: '100vh', }}
            >
                <Sider collapsible collapsed={collapsed} onCollapse={(value) => setCollapsed(value)}>
                    <div className="logo mt-4 flex" >
                        <img src='https://i.pinimg.com/originals/ce/a7/c1/cea7c1c3377295b4e3ba605488ea3741.png' alt='Phonix' className='w-14 h-14 cursor-pointer ml-4' />
                        <span className='text-yellow-500 flex items-center font-bold text-lg mx-4'>PHOENIX</span>
                    </div>
                    <Menu theme="dark" defaultSelectedKeys={['1']} mode="inline">
                        <Menu.Item key='1' icon={<UserOutlined />}>
                            <NavLink to='/admin/users'>Users</NavLink>
                        </Menu.Item>
                        {/* <SubMenu key='sub2' icon={<FileOutlined />} title='Films' > */}
                        <Menu.Item key='2' icon={<FileOutlined />}>
                            <NavLink to='/admin/films'>Films</NavLink>
                        </Menu.Item>
                        {/* <Menu.Item key='3' >
                                <NavLink to='/admin/films/add'>Thêm Film</NavLink>
                            </Menu.Item>
                            <Menu.Item key='4' >
                                <NavLink to='/admin/films/add'>Sửa Film</NavLink>
                            </Menu.Item>
                            <Menu.Item key='5' >
                                <NavLink to='/admin/films/add'>Xóa Film</NavLink>
                            </Menu.Item> */}
                        {/* </SubMenu> */}

                        {/* <Menu.Item key='3' icon={<DesktopOutlined />}>
                            <NavLink to='/admin/showtimes'>Showtime</NavLink>
                        </Menu.Item> */}
                    </Menu>
                </Sider>
                <Layout className="site-layout">
                    <Header className="site-layout-background" style={{ padding: 0, }} >
                        <div className='flex justify-end my-4'>{operations}</div>
                    </Header>
                    <Content style={{ margin: '0 16px', }}>
                        <Breadcrumb style={{ margin: '16px 0', }}>
                            {/* <Breadcrumb.Item>User</Breadcrumb.Item>
                            <Breadcrumb.Item>Bill</Breadcrumb.Item> */}
                        </Breadcrumb>
                        <div className="site-layout-background" style={{ padding: 24, minHeight: 360, }}>

                            <Component {...propsRoute} />

                        </div>
                    </Content>
                    <Footer style={{ textAlign: 'center', }}>
                        Ant Design ©2018 Created by Ant UED
                    </Footer>
                </Layout>
            </Layout>
        </Fragment>
    }} />
};


