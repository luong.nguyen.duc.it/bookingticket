import { quanLyDatVeService } from "../../services/QuanLyDatVeService";
import { SET_DAT_VE, DAT_VE_HOAN_TAT } from './../types/QuanLyDatVeType';
import { ThongTinDatVe } from './../../_core/modules/ThongTinDatVe';
import { displayLoadingAction, hideLoadingAction } from "./LoadingAction";
import { message } from 'antd';
import { history } from './../../App';

export const LayChiTietPhongVeAction = (maLichChieu) => {
    return async dispatch => {
        try {
            const result = await quanLyDatVeService.layChiTietPhongVe(maLichChieu);
            if (result.status === 200) {
                dispatch({
                    type: SET_DAT_VE,
                    chiTiet: result.data.content
                })
            }
        } catch (error) {
            console.log('error', error.response?.data)
        }
    }
}



export const datVeAction = (thongTinDatVe = new ThongTinDatVe()) => {

    return async dispatch => {
        try {
            dispatch(displayLoadingAction)
            // Dat ve thanh cong
            const result = await quanLyDatVeService.datVe(thongTinDatVe);
            // load lai phong ve
            await dispatch(LayChiTietPhongVeAction(thongTinDatVe.maLichChieu));
            // khong phai load lai api ghe
            dispatch({ type: DAT_VE_HOAN_TAT })
            await dispatch(hideLoadingAction)

        } catch (error) {
            console.log('error', error.response?.data)
        }
    }
}


export const taoLichChieuAction = (data) => {
    return async dispatch => {
        try {
            const result = await quanLyDatVeService.taoLichChieu(data);

            message.success('Tạo thành công!')
            history.push('/admin/films')
        } catch (error) {
            console.log(error.response?.data)
        }
    }
}