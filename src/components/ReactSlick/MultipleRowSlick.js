import React from 'react'
import Slider from "react-slick";
import styleSlick from './MultipleRow.module.css'
import Film from '../Film/Film'
import { useDispatch, useSelector } from 'react-redux';
import { SET_FILM_SAP_CHIEU, SET_FILM_DANG_CHIEU, } from './../../redux/types/FilmType';
function SampleNextArrow(props) {
    const { className, style, onClick } = props;
    return (
        <div className={`${className} ${styleSlick['slick-prev']}`}
            style={{ ...style, display: 'block' }}
            onClick={onClick}
        >
        </div>
    )
}

function SamplePrevArrow(props) {
    const { className, style, onClick } = props;
    return (
        <div
            className={`${className} ${styleSlick['slick-prev']}`}
            style={{ ...style, display: 'block', left: '-50px' }}
            onClick={onClick}
        ></div>
    )
}

export default function MultipleRowSlick(props) {

    const dispatch = useDispatch();

    const { dangChieu, sapChieu } = useSelector(state => state.QuanLyFilmReducer);

    let classActiveDangChieu = dangChieu === true ? 'active_Film' : 'none_Dang_Chieu';

    let classActiveSapChieu = sapChieu === true ? 'active_Film' : 'node_Sap_Chieu';

    const renderFilm = () => {
        return props.lstFilm.map((item, index) => {
            return <Film key={index} film={item} />

        })
    }

    const settings = {
        className: "center variable-width",
        centerMode: true,
        infinite: true,
        centerPadding: "60px",
        slidesToShow: 3,
        speed: 500,
        rows: 1,
        slidesPerRow: 2,
        variableWidth: true,
        nextArrow: <SampleNextArrow />,
        prevArrow: <SamplePrevArrow />
    };
    return (
        <div>
            <div>
                <button type="button" className={`${styleSlick[classActiveDangChieu]} w-1/6 h-12 border border-green-900 text-yellow-400 text-lg font-bold rounded-md px-4 py-2 m-2 transition duration-500 ease select-none hover:bg-green-900 hover:text-white focus:outline-none focus:shadow-outline`} onClick={() => {
                    const action = { type: SET_FILM_DANG_CHIEU }
                    dispatch(action);
                    console.log(dangChieu)
                }}>PHIM ĐANG CHIẾU</button>
                <button type="button" className={`${styleSlick[classActiveSapChieu]} w-1/6 h-12 text-lg font-bold border border-green-900 text-red-400 rounded-md px-4 py-2 m-2 transition duration-500 ease select-none hover:bg-green-900 hover:text-white focus:outline-none focus:shadow-outline`} onClick={() => {
                    const action = { type: SET_FILM_SAP_CHIEU }
                    dispatch(action)
                }}>PHIM SẮP CHIẾU</button>
            </div>


            <Slider {...settings}>
                {renderFilm()}
            </Slider>
        </div>
    )
}
