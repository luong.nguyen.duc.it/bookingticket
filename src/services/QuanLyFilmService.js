
import { baseService } from './baseServices';

import { GROUP } from '../utils/settings/config'

export class QuanLyFilmService extends baseService {

    // eslint-disable-next-line no-useless-constructor
    constructor() {
        super();
    }

    // eslint-disable-next-line no-unreachable
    layDanhSachBanner = () => {
        return this.get(`/api/QuanLyPhim/LayDanhSachBanner`);
    }

    layDanhSachFilm = () => {
        return this.get(`/api/QuanLyPhim/LayDanhSachPhim?maNhom=${GROUP}`)
    }

    themFilmUpLoadHinh = (dataFilm) => {
        return this.post(`/api/QuanLyPhim/ThemPhimUploadHinh`, dataFilm)
    }

    xoaFilm = (id) => {
        return this.delete(`/api/QuanLyPhim/XoaPhim?maPhim=${id}`)
    }

    getFilm = (id) => {
        return this.get(`/api/QuanLyPhim/LayThongTinPhim?maPhim=${id}`)
    }
    suaFilm = (data) => {
        return this.post(`/api/QuanLyPhim/CapNhatPhimUpload`, data)
    }
}

export const quanLyFilmService = new QuanLyFilmService();