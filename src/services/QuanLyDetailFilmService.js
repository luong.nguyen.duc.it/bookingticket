


import { baseService } from './baseServices';


export class QuanLyDetailFilmService extends baseService {

    // eslint-disable-next-line no-useless-constructor
    constructor() {
        super();
    }

    // eslint-disable-next-line no-unreachable
    layDetailFilm = (maPhim) => {
        return this.get(`/api/QuanLyRap/LayThongTinLichChieuPhim?MaPhim=${maPhim}`);
    }

}

export const quanLyDetailFilmService = new QuanLyDetailFilmService();