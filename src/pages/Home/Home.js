import React, { useEffect } from 'react'
import HomeMenu from './HomeMenu/HomeMenu'
import { useDispatch, useSelector } from 'react-redux';
import { getFilmAction } from '../../redux/actions/QuanLyFilmACtion';
import MultipleRowSlick from '../../components/ReactSlick/MultipleRowSlick';
import { getRapAction } from '../../redux/actions/QuanLyRapAction';
import HomeCarousel from './../../templates/HomeTemplate/Layout/HomeCarousel/HomeCarousel';

export default function Home(props) {

    const dispatch = useDispatch();

    const { lstFilm } = useSelector(state => state.QuanLyFilmReducer);

    const { lstRapChieu } = useSelector(state => state.QuanLyRapReducer);
    useEffect(() => {
        dispatch(getFilmAction())

        dispatch(getRapAction())
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])




    return (
        <div>
            <HomeCarousel />

            <section className="text-gray-600 body-font">
                <div className="container px-5 py-24 mx-auto">
                    <MultipleRowSlick lstFilm={lstFilm} />
                </div>
            </section>

            <div className='px-64'>
                <HomeMenu lstRapChieu={lstRapChieu} />
            </div>
        </div>
    )
}
