import React from 'react'
import { useDispatch } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { useFormik } from 'formik'
import * as Yup from "yup";

import { dangKyAction } from '../../redux/actions/QuanLyNguoiDungAction';
import { GROUP } from '../../utils/settings/config';


export default function Register(props) {
    const dispatch = useDispatch();


    const formik = useFormik({
        initialValues: {
            email: '',
            taiKhoan: '',
            soDt: '',
            matKhau: '',
            maNhom: GROUP,
            hoTen: '',
        },
        validationSchema: Yup.object({
            taiKhoan: Yup.string()
                .min(2, "Too Short!")
                .max(50, "Too Long!")
                .required("Account is required"),
            email: Yup.string()
                .min(2, "Too Short!")
                .max(50, "Too Long!")
                .required("Email is required"),
            matKhau: Yup.string()
                .min(2, "Too Short!")
                .max(50, "Too Long!")
                .required("Password is required"),
            soDt: Yup.string()
                .min(2, "Too Short!")
                .max(50, "Too Long!")
                .required("Phone Number is required"),
            hoTen: Yup.string()
                .min(2, "Too Short!")
                .max(50, "Too Long!")
                .required("Name is required"),

        }),
        onSubmit: values => {
            dispatch(dangKyAction(values))
        },
    });
    return (
        <div className="lg:w-1/2 xl:max-w-screen-sm">
            <div className="py-12 bg-indigo-100 lg:bg-white flex justify-center lg:justify-start lg:px-12">
                <div className="cursor-pointer flex items-center">
                    <NavLink to='/home'>
                        <img src='https://i.pinimg.com/originals/ce/a7/c1/cea7c1c3377295b4e3ba605488ea3741.png' alt='PHOENIX' className='h-36 w-36' />
                    </NavLink>
                    <div className="text-2xl text-yellow-500 tracking-wide ml-2 font-bold">PHOENIX</div>
                </div>
            </div>
            <div className="mt-10 px-12 sm:px-24 md:px-48 lg:px-12 lg:mt-16 xl:px-24 xl:max-w-2xl">
                <h2 className="text-center text-4xl text-yellow-500 font-display font-semibold lg:text-left xl:text-5xl
              xl:text-bold">Đăng ký</h2>
                <div className="mt-12">
                    <form onSubmit={formik.handleSubmit}>
                        <div>
                            <div className="text-lg font-bold text-green-900 tracking-wide">Email</div>
                            <input type='email' name='email' onChange={formik.handleChange} className="w-full text-lg py-2 border-b border-gray-300 focus:outline-none focus:border-yellow-500" placeholder="Nhập email..." />
                            {formik.errors.email && formik.touched.email && (
                                <span className='text-red-400'>{formik.errors.email}</span>
                            )}
                        </div>

                        <div className='mt-4'>
                            <div className="text-lg font-bold text-green-900 tracking-wide">Tài khoản</div>
                            <input type='text' name='taiKhoan' onChange={formik.handleChange} className="w-full text-lg py-2 border-b border-gray-300 focus:outline-none focus:border-yellow-500" placeholder="Nhập tài khoản..." />
                            {formik.errors.taiKhoan && formik.touched.taiKhoan && (
                                <span className='text-red-400'>{formik.errors.taiKhoan}</span>
                            )}
                        </div>

                        <div className='mt-4'>
                            <div className="text-lg font-bold text-green-900 tracking-wide">Số điện thoại</div>
                            <input type='number' name='soDt' onChange={formik.handleChange} className="w-full text-lg py-2 border-b border-gray-300 focus:outline-none focus:border-yellow-500" placeholder="Nhập số điện thoại..." />
                            {formik.errors.soDt && formik.touched.soDt && (
                                <span className='text-red-400'>{formik.errors.soDt}</span>
                            )}
                        </div>

                        <div className="mt-4">
                            <div className="text-lg font-bold text-green-900 tracking-wide">Mật khẩu</div>
                            <input type='password' name='matKhau' onChange={formik.handleChange} className="w-full text-lg py-2 border-b border-gray-300 focus:outline-none focus:border-yellow-500" placeholder="Nhập mật khẩu..." />
                            {formik.errors.matKhau && formik.touched.matKhau && (
                                <span className='text-red-400'>{formik.errors.matKhau}</span>
                            )}
                        </div>
                        <div className='mt-4'>
                            <div className="text-lg font-bold text-green-900 tracking-wide">Họ Tên</div>
                            <input type='text' name='hoTen' onChange={formik.handleChange} className="w-full text-lg py-2 border-b border-gray-300 focus:outline-none focus:border-yellow-500" placeholder="Nhập họ và tên..." />
                            {formik.errors.hoTen && formik.touched.hoTen && (
                                <span className='text-red-400'>{formik.errors.hoTen}</span>
                            )}
                        </div>
                        <div className="mt-10">
                            <button className="text-yellow-500 text-xl font-bold border-2 border-green-900 p-4 w-full rounded-full tracking-wide
                          font-semibold font-display focus:outline-none focus:shadow-outline hover:bg-green-900 hover:text-white
                          shadow-2xl">
                                Đăng ký
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    )
}
