


import { baseService } from './baseServices';
import { ThongTinDatVe } from './../_core/modules/ThongTinDatVe';


export class QuanLyDatVeService extends baseService {

    // eslint-disable-next-line no-useless-constructor
    constructor() {
        super();
    }

    // eslint-disable-next-line no-unreachable
    layChiTietPhongVe = (maLichChieu) => { // lay tu url
        return this.get(`/api/QuanLyDatVe/LayDanhSachPhongVe?MaLichChieu=${maLichChieu}`);
    }

    datVe = (thongTinDatVe = new ThongTinDatVe()) => {
        return this.post(`/api/QuanLyDatVe/DatVe`, thongTinDatVe)
    }
    taoLichChieu = (data) => {
        return this.post(`/api/QuanLyDatVe/TaoLichChieu`, data)
    }
}

export const quanLyDatVeService = new QuanLyDatVeService();