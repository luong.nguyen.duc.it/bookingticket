import React, { Fragment, useState, useEffect } from 'react'
import {
    DatePicker,
    Input,
    InputNumber,
    Switch,
} from 'antd';
import { useFormik } from 'formik'
import * as Yup from "yup";
import { useDispatch, useSelector } from 'react-redux';
import moment from 'moment';
import { layFilmAction, editFilmAction } from '../../../../redux/actions/QuanLyFilmACtion';
import { GROUP } from '../../../../utils/settings/config';

export default function EditFilm(props) {
    const { filmEdit } = useSelector(state => state.QuanLyFilmReducer);
    // console.log({ filmEdit })

    const { TextArea } = Input;

    const [img, setImg] = useState('');

    const dispatch = useDispatch();

    useEffect(() => {
        let { id } = props.match.params;
        dispatch(layFilmAction(id));
    }, [])

    const formik = useFormik({
        enableReinitialize: true,
        initialValues: {
            maPhim: filmEdit.maPhim,
            tenPhim: filmEdit.tenPhim,
            trailer: filmEdit.trailer,
            moTa: filmEdit.moTa,
            ngayKhoiChieu: filmEdit.ngayKhoiChieu,
            dangChieu: filmEdit.dangChieu,
            sapChieu: filmEdit.sapChieu,
            hot: filmEdit.hot,
            danhGia: filmEdit.danhGia,
            hinhAnh: null,
        },
        // validationSchema: Yup.object({
        //     taiKhoan: Yup.string()
        //         .min(2, "Too Short!")
        //         .max(50, "Too Long!")
        //         .required("Account is required"),
        //     matKhau: Yup.string()
        //         .min(2, "Too Short!")
        //         .max(50, "Too Long!")
        //         .required("Password is required"),

        // }),
        onSubmit: values => {
            values.maNhom = GROUP;
            //tạo đối tượng formdata => đưa values từ formik vào formdata
            let data = new FormData();
            for (let key in values) {
                if (key !== 'hinhAnh') {
                    data.append(key, values[key]);
                }
                else {
                    if (values.hinhAnh !== null) {
                        data.append('File', values.hinhAnh, values.hinhAnh.name);
                    }
                }
            }
            dispatch(editFilmAction(data));
        },
    });

    const handleChangeDatePicker = (value) => {
        let ngayKhoiChieu = moment(value);
        formik.setFieldValue('ngayKhoiChieu', ngayKhoiChieu)
    }
    const handleChangeSwitch = (name) => {
        return (value) => { formik.setFieldValue(name, value) }
    }

    const handleChangeFile = async (e) => {

        let file = e.target.files[0]

        if (file.type === 'image/jpeg' || file.type === 'image/jpeg' || file.type === 'image/jpg' || file.type === 'image/png') {
            await formik.setFieldValue('hinhAnh', file)
            let reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = (e) => {
                setImg(e.target.result) //Hinh base 64
            }

        }

    }

    return (
        <Fragment>
            <h2 className='text-4xl font-bold text-center text-yellow-500'>Sửa Film</h2>
            <form onSubmit={formik.handleSubmit}>
                <div className='grid grid-cols-12 my-4'>
                    <span className='col-start-4'>Tên Phim:</span>
                    <Input className='col-span-4' name='tenPhim' onChange={formik.handleChange} value={formik.values.tenPhim} />
                </div>
                <div className='grid grid-cols-12 my-4'>
                    <span className='col-start-4'>Trailer:</span>
                    <Input className='col-span-4' name='trailer' onChange={formik.handleChange} value={formik.values.trailer} />
                </div>
                <div className='grid grid-cols-12 my-4'>
                    <span className='col-start-4'>Mô tả:</span>
                    <TextArea className='col-span-4' name='moTa' rows={4} onChange={formik.handleChange} value={formik.values.moTa} />
                </div>
                <div className='grid grid-cols-12 my-4'>
                    <span className='col-start-4'>Ngày khởi chiếu:</span>
                    <DatePicker onChange={handleChangeDatePicker} format={'DD/MM/YYYY'} defaultValue={moment(formik.values.ngayKhoiChieu)} />
                </div>
                <div className='grid grid-cols-12 my-4'>
                    <span className='col-start-4'>Đang chiếu:</span>
                    <Switch className='w-1/3' onChange={handleChangeSwitch('dangChieu')} checked={formik.values.dangChieu} />
                </div>
                <div className='grid grid-cols-12 my-4'>
                    <span className='col-start-4'>Sắp chiếu:</span>
                    <Switch className='w-1/3' onChange={handleChangeSwitch('sapChieu')} checked={formik.values.sapChieu} />
                </div>
                <div className='grid grid-cols-12 my-4'>
                    <span className='col-start-4'>Hot:</span>
                    <Switch className='w-1/3' onChange={handleChangeSwitch('hot')} checked={formik.values.hot} />
                </div>
                <div className='grid grid-cols-12 my-4'>
                    <span className='col-start-4'>Đánh giá:</span>
                    <InputNumber onChange={(value) => { formik.setFieldValue('danhGia', value) }} min={1} max={10} value={formik.values.danhGia} />
                </div>
                <div className='grid grid-cols-12 my-4'>
                    <span className='col-start-4'>Hình ảnh:</span>
                    <input className='col-span-2' type='file' onChange={handleChangeFile} accept='image/jpeg, image/jpg, image/png' />
                </div>
                <div className='grid grid-cols-12 my-4'>
                    <img className='col-start-5 w-26 h-36 rounded-md' src={img === '' ? filmEdit.hinhAnh : img} alt='...' />
                </div>


                <div className='text-center mt-24'>
                    <button type='submit' className="w-36 h-12 text-yellow-500 text-xl font-bold border-2 border-green-900 rounded hover:bg-green-900 hover:text-white">Cập nhật</button>
                </div>
            </form >
        </Fragment >
    )
}
