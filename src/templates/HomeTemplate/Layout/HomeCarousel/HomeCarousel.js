import React, { useEffect } from 'react'
import { Carousel } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { getBannerAction } from './../../../../redux/actions/QuanLyFilmACtion';
import './HomeCarousel.css'
const contentStyle = {
    height: '625px',
    color: '#fff',
    lineHeight: '160px',
    textAlign: 'center',
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat'
};



export default function HomeCarousel(props) {
    const dispatch = useDispatch();
    const { lstBanner } = useSelector(state => state.QuanLyCarouselReducer)
    useEffect(() => {
        dispatch(getBannerAction())
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])
    const renderBanner = () => {
        return lstBanner.map((item, index) => {
            return <div key={index}>
                <div style={contentStyle}>
                    <img src={item.hinhAnh} alt='123' className='w-full' />
                </div>
            </div>
        })
    }

    return (
        <Carousel autoplay>
            {renderBanner()}
        </Carousel>
    )
}
