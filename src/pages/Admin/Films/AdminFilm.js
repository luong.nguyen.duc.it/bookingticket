import React, { Fragment, useEffect } from 'react'
import { Table } from 'antd';
import { EditOutlined, DeleteOutlined, ScheduleOutlined } from '@ant-design/icons';
import { history } from '../../../App';
import { useDispatch, useSelector } from 'react-redux';
import { Input } from 'antd';
import { dellFilmAction, getFilmAction } from '../../../redux/actions/QuanLyFilmACtion'
import _ from 'lodash';

const { Search } = Input;
const onSearch = (value) => console.log(value);
export default function AdminFilm() {
    const dispatch = useDispatch();

    const { lstFilmDefault } = useSelector(state => state.QuanLyFilmReducer);
    useEffect(() => {
        dispatch(getFilmAction())
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    const columns = [
        {
            title: 'ID',
            dataIndex: 'maPhim',
            value: (text, object) => { return <span>{text}</span> },
            sorter: (a, b) => a.maPhim - b.maPhim,
            sortDirections: ['descend', 'ascend']
        },
        {
            title: 'Tên Phim',
            dataIndex: 'tenPhim',
            render: (text, film) => {
                return <Fragment>
                    <span className='font-bold text-lg text-yellow-500'>{film.tenPhim}</span>
                </Fragment>
            },
            width: 300
        },
        {
            title: 'Hình ảnh',
            dataIndex: 'hinhAnh',
            render: (text, film) => {
                return <Fragment>
                    <img className='rounded-md w-36' src={film.hinhAnh} alt={film.tenPhim} />
                </Fragment>
            }
        },
        {
            title: 'Mô tả',
            dataIndex: 'moTa',
            render: (text, film) => {
                return <Fragment>
                    <span className='flex flex-wrap'>{_.truncate(film.moTa, { 'length': 200 })}</span>
                </Fragment>
            },
            width: 400
        },
        {
            title: 'Mã nhóm',
            dataIndex: 'maNhom',
        },
        {
            title: '',
            dataIndex: 'id',
            render: (text, record) => {
                return <div className='flex'>
                    <button className='mx-4 text-green-500 hover:text-4xl' title='Sửa' onClick={() => {
                        history.push(`/admin/films/edit/${record.maPhim}`)
                    }}>
                        <EditOutlined style={{ fontSize: 25 }} />
                    </button>
                    <button className='mx-4 text-red-500 hover:text-4xl' title='Xóa' onClick={() => {
                        dispatch(dellFilmAction(record.maPhim))
                    }}>
                        <DeleteOutlined style={{ fontSize: 25 }} />
                    </button>
                    <button className='mx-4 text-yellow-500 hover:text-4xl' title='Lịch chiếu' onClick={() => {
                        history.push(`/admin/films/showtime/${record.maPhim}`)
                    }}>
                        <ScheduleOutlined style={{ fontSize: 25 }} />
                    </button>
                </div>
            },
        },
    ];
    return (
        <div className='container mt-4'>
            <h2 className='text-4xl font-bold text-center text-yellow-500'>Quản lý Film</h2>
            <div className='mt-10 flex justify-between'>
                <button type='button' className='border-2 border-green-900 rounded w-24 h-10 text-lg font-bold text-yellow-500 hover:text-white hover:bg-green-900' onClick={() => {
                    history.push('/admin/films/add')
                }}>Thêm </button>
                <div className='w-1/2'>
                    <Search size='large' placeholder="Bạn muốn tìm gì?..." onSearch={onSearch} enterButton />
                </div>
            </div>
            <Table columns={columns} dataSource={lstFilmDefault} rowKey='maPhim' />
        </div>
    )
}
