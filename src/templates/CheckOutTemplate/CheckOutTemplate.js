
import { Route } from 'react-router';
import { USER_LOGIN } from './../../utils/settings/config'
import { Redirect } from 'react-router-dom';
import { useEffect } from 'react';

export const CheckOutTemplate = (props) => {
    const { Component, ...restRoute } = props;
    useEffect(() => {
        window.scrollTo(0, 0);
    })

    if (!sessionStorage.getItem(USER_LOGIN)) {
        return <Redirect to='/login' />
    }

    return <Route {...restRoute} render={(propsRoute) => {

        return <>

            <Component {...propsRoute} />

        </>
    }} />
}