import { quanLyNguoiDungService } from "../../services/QuanLyNguoiDungService"
import { SET_DANG_NHAP, SET_GET_LIST_USER, SET_THONG_TIN_NGUOI_DUNG } from './../types/QuanLyNguoiDungType';
import { history } from '../../App'
import { message } from 'antd';

export const dangKyAction = (thongTinDangKy) => {
    return async dispatch => {
        try {
            const result = await quanLyNguoiDungService.signUp(thongTinDangKy);
            if (result.status === 200) {
                await message.success("Đăng ký tài khoản thành công!")
                history.push('/login')
            }
            else {
                message.error("Đăng ký thất bại!")
            }
        } catch (error) {
            console.log('error', error.response?.data)
        }
    }
}

export const dangNhapAction = (thongTinDangNhap) => {
    return async dispatch => {
        try {
            const result = await quanLyNguoiDungService.guiThongTinDangNhap(thongTinDangNhap);
            if (result.status === 200) {
                dispatch({
                    type: SET_DANG_NHAP,
                    thongTinDangNhap: result.data.content
                })
                message.success("Đăng nhập thành công!")
                if (result.data.content.maLoaiNguoiDung !== 'QuanTri') {
                    history.push('/home');
                } else {
                    history.push('/admin')
                }

            }


        } catch (error) {
            message.error("Sai tài khoản hoặc mật khẩu!")
            console.log('error', error.response?.data)
        }
    }
}

export const layThongTinNguoiDungAction = () => {
    return async dispatch => {
        try {
            const result = await quanLyNguoiDungService.layThongTinNguoiDung();
            if (result.status === 200) {
                dispatch({
                    type: SET_THONG_TIN_NGUOI_DUNG,
                    thongTinUser: result.data.content
                })
            }


        } catch (error) {
            console.log('error', error.response?.data)

        }
    }
}


export const getListUserAction = () => {
    return async dispatch => {
        try {
            const result = await quanLyNguoiDungService.getlistUser();
            if (result.status === 200) {
                dispatch({
                    type: SET_GET_LIST_USER,
                    listUsers: result.data.content
                })
            }
        } catch (error) {
            console.log('error', error.response?.data)
        }
    }
}
