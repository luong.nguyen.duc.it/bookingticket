import { TOKEN, USER_LOGIN } from "../../utils/settings/config";
import { SET_DANG_NHAP, SET_GET_LIST_USER, SET_THONG_TIN_NGUOI_DUNG } from "../types/QuanLyNguoiDungType"
import { ThongTinNguoiDung } from './../../_core/modules/ThongTinNguoiDung';

let userDefault = {};

if (sessionStorage.getItem(USER_LOGIN)) {
    userDefault = JSON.parse(sessionStorage.getItem(USER_LOGIN));

}

const initialState = {
    userLogin: userDefault,
    thongTinNguoiDung: new ThongTinNguoiDung(),
    listUsers: []
}

// eslint-disable-next-line import/no-anonymous-default-export
export const QuanLyNguoiDungReducer = (state = initialState, action) => {
    switch (action.type) {

        case SET_DANG_NHAP: {
            const { thongTinDangNhap } = action;
            sessionStorage.setItem(USER_LOGIN, JSON.stringify(thongTinDangNhap));
            sessionStorage.setItem(TOKEN, thongTinDangNhap.accessToken);
            return { ...state, userLogin: thongTinDangNhap }
        }

        case SET_THONG_TIN_NGUOI_DUNG: {
            state.thongTinNguoiDung = action.thongTinUser;
            return { ...state }
        }
        case SET_GET_LIST_USER: {
            state.listUsers = action.listUsers;
            return { ...state }
        }
        default:
            return state
    }
}

