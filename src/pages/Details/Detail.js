import React, { useState, useEffect } from 'react'
import { CustomCard } from '@tsamantanis/react-glassmorphism'
import '@tsamantanis/react-glassmorphism/dist/index.css'
import { useSelector, useDispatch } from 'react-redux';
import './../../assets/styles/CirclePercentage.css'
import { Rate, Tabs } from 'antd';
import { getDetailFilmAction } from './../../redux/actions/QuanLyFilmACtion';
import moment from 'moment'
import { NavLink } from 'react-router-dom';

const { TabPane } = Tabs;


export default function Detail(props) {

    const { filmDetail } = useSelector(state => state.QuanLyFilmReducer)


    const dispatch = useDispatch();


    useEffect(() => {
        let { id } = props.match.params;
        dispatch(getDetailFilmAction(id))
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])



    const [tabPosition] = useState('left');

    return (
        <div style={{ backgroundImage: `url(${filmDetail.hinhAnh})`, backgroundRepeat: 'no-repeat', backgroundSize: '100%', }}>
            <CustomCard
                effectColor="#eab308"
                blur={10}
                borderRadius={0}
            >
                <div className='grid grid-cols-12 pt-36'>
                    <div className='col-span-4 col-start-4 flex'>
                        <img className='h-72 w-60 rounded-md' src={filmDetail.hinhAnh} alt={filmDetail.tenPhim} />
                        <div className='ml-6 flex flex-col'>
                            <span className='text-2xl font-bold text-green-600'>{filmDetail.tenPhim}</span>
                            <span className='text-white'>
                                Ngày chiếu:
                                <span className='text-yellow-500 font-bold text-base ml-2'>
                                    {moment(filmDetail.ngayKhoiChieu
                                    ).format('DD/MM/YYYY')}
                                </span>
                            </span>
                            <span className='text-white'>{filmDetail.moTa}</span>
                        </div>
                    </div>

                    <div className='col-span-4 ml-28'>
                        <div className='text-yellow-500 font-bold text-lg ml-6'>Đánh Giá</div>
                        <div className='mb-2'>
                            <Rate allowHalf value={filmDetail.danhGia / 2} />
                        </div>

                        <div style={{ marginLeft: '1.3%' }} className={`c100 span${filmDetail.danhGia * 10} green`}>
                            <span style={{ color: '#eab308', fontWeight: 600 }}>{filmDetail.danhGia}</span>
                            <div className="slice">
                                <div className="bar" />
                                <div className="fill" />
                            </div>
                        </div>
                    </div>

                </div>

                <Tabs defaultActiveKey='1' centered>
                    <TabPane tab={<span className='font-bold text-xl text-yellow-500'>Lịch Chiếu</span>} key='1'>
                        <div className='span-4 mx-44 bg-gray-900 rounded-md '>
                            <Tabs tabPosition={tabPosition}>
                                {/* He thong cum rap */}
                                {filmDetail.heThongRapChieu?.map((heThongRap, index) => {
                                    return <TabPane key={index} tab={<img src={heThongRap.logo} alt='' className='rounded-full h-16 w-16' />}>
                                        <Tabs tabPosition={tabPosition}>
                                            {/* Thong tin cum rap */}
                                            {heThongRap.cumRapChieu?.map((cumRap, index) => {
                                                return <TabPane key={index} tab={<div className='flex'>
                                                    <img src={cumRap.hinhAnh} alt='' className='rounded h-16 w-16' />
                                                    <div className='ml-4 flex flex-col'>
                                                        <span className='text-green-900 font-bold text-xl'>
                                                            {cumRap.tenCumRap}
                                                        </span>
                                                        <span className='text-start text-yellow-500'>
                                                            {cumRap.diaChi}
                                                        </span>
                                                    </div>
                                                </div>
                                                }>
                                                    {/* lich chieu */}
                                                    {cumRap.lichChieuPhim?.slice(0, 5).map((lichChieu, index) => {
                                                        return <div key={index} >

                                                            <div className='grid grid-cols-4' >
                                                                <div className='my-5 col-span-3'>
                                                                    <span className='text-green-900 font-bold text-xl'>
                                                                        {lichChieu.tenRap}
                                                                    </span>
                                                                    <span className='flex text-green-900 font-bold text-xl m-0'>
                                                                        Ngày Chiếu:
                                                                        <span className='ml-4 text-yellow-500'>
                                                                            {moment(lichChieu.ngayChieuGioChieu).format('DD/MM/YYYY')}
                                                                        </span>
                                                                        <span className='ml-4 text-green-900'>
                                                                            {moment(lichChieu.ngayChieuGioChieu).format('hh:mm A')}
                                                                        </span>
                                                                    </span>
                                                                    <span className='flex text-green-900 font-bold text-xl m-0'>

                                                                        Giá Vé:
                                                                        <span className='mx-4 text-yellow-500'>
                                                                            {lichChieu.giaVe}
                                                                        </span>
                                                                        đồng
                                                                    </span>
                                                                    <span className='flex text-green-900 font-bold text-xl m-0'>
                                                                        Thời lượng:
                                                                        <span className='mx-4 text-yellow-500'>
                                                                            {lichChieu.thoiLuong}
                                                                        </span>
                                                                        phút
                                                                    </span>
                                                                </div>
                                                                <NavLink to={`/checkout/${lichChieu.maLichChieu}`}>
                                                                    <button type="button" className="border h-12 w-36 font-bold text-base border-green-900 text-yellow-600 rounded-md px-4 py-2 m-2 transition duration-500 ease select-none 
                                                        bg-white hover:bg-green-900 hover:text-white focus:outline-none focus:shadow-outline">
                                                                        Đặt Vé
                                                                    </button>
                                                                </NavLink>

                                                            </div>

                                                            <hr />

                                                        </div>
                                                    })}
                                                </TabPane>

                                            })}
                                        </Tabs>

                                    </TabPane>

                                })}

                            </Tabs>
                        </div>
                    </TabPane>
                    <TabPane tab={<span className='font-bold text-xl text-yellow-500'>Thông Tin</span>} key='2'>
                        <div className='span-4 mx-48 bg-gray-900 rounded-md h-96 w-4/5 text-green-900 font-bold text-3xl '>
                            Thông Tin
                        </div>
                    </TabPane>
                    <TabPane tab={<span className='font-bold text-xl text-yellow-500'>Đánh Giá</span>} key='3'>
                        <div className='span-4 mx-48 bg-gray-900 rounded-md h-96 w-4/5 text-green-900 font-bold text-3xl '>
                            Đánh Giá
                        </div>
                    </TabPane>
                </Tabs>



            </CustomCard >


        </div >
    )
}
