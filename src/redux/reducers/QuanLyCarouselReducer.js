import { SET_LIST_BANNER } from '../types/CarouselType'

const initialState = {
    lstBanner: []
}

// eslint-disable-next-line import/no-anonymous-default-export
export const QuanLyCarouselReducer = (state = initialState, action) => {
    switch (action.type) {

        case SET_LIST_BANNER: {
            return { ...state, lstBanner: action.lstBanner }
        }

        default:
            return { ...state }
    }
}
