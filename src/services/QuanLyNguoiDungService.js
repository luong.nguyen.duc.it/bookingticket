
import { GROUP } from '../utils/settings/config';
import { baseService } from './baseServices';


export class QuanLyNguoiDungService extends baseService {

    // eslint-disable-next-line no-useless-constructor
    constructor() {
        super();
    }

    // eslint-disable-next-line no-unreachable
    guiThongTinDangNhap = (thongTinDangNhap) => { //tai khoan + mat khau
        return this.post(`/api/QuanLyNguoiDung/DangNhap`, thongTinDangNhap);
    }

    signUp = (thongTinDangKy) => {
        return this.post(`/api/QuanLyNguoiDung/DangKy`, thongTinDangKy)
    }

    layThongTinNguoiDung = () => {
        return this.post(`/api/QuanLyNguoiDung/ThongTinTaiKhoan`)
    }

    getlistUser = () => {
        return this.get(`/api/QuanLyNguoiDung/LayDanhSachNguoiDung?MaNhom=${GROUP}`)
    }
}

export const quanLyNguoiDungService = new QuanLyNguoiDungService();