import { quanLyDetailFilmService } from '../../services/QuanLyDetailFilmService';
import { SET_LIST_BANNER } from '../types/CarouselType';
import { quanLyFilmService } from './../../services/QuanLyFilmService';
import { SET_LIST_FILM, SET_DETAIL_FILM, SET_EDIT_FILM } from './../types/FilmType';
import { message } from 'antd';
import { history } from '../../App';


export const getBannerAction = () => {

    return async dispatch => {
        try {
            const result = await quanLyFilmService.layDanhSachBanner();
            if (result.status === 200) {
                dispatch({
                    type: SET_LIST_BANNER,
                    lstBanner: result.data.content
                })
            }
        }
        catch (error) {
            console.log(error)
        }
    }
}

export const getFilmAction = () => {
    return async dispatch => {
        try {
            const result = await quanLyFilmService.layDanhSachFilm();
            if (result.status === 200) {
                dispatch({
                    type: SET_LIST_FILM,
                    lstFilm: result.data.content
                })
            }
        } catch (error) {
            console.log(error)
        }
    }
}


export const getDetailFilmAction = (id) => {
    return async dispatch => {
        try {
            const result = await quanLyDetailFilmService.layDetailFilm(id);
            if (result.status === 200) {
                dispatch({
                    type: SET_DETAIL_FILM,
                    filmDetail: result.data.content
                })
            }
        } catch (error) {
            console.log(error)
        }
    }
}


export const addFilmAction = (dataFilm) => {
    return async dispatch => {
        try {
            let result = await quanLyFilmService.themFilmUpLoadHinh(dataFilm);
            message.success('Thêm mới thành công!')
            history.push('/admin/films')
        } catch (error) {
            console.log(error.reponse?.data)
        }
    }
}
export const editFilmAction = (data) => {
    return async dispatch => {
        try {
            let result = await quanLyFilmService.suaFilm(data);
            message.success('Cập nhật thành công!')
            history.push('/admin/films')
        } catch (error) {
            console.log(error.reponse?.data)
        }
    }
}

export const dellFilmAction = (id) => {

    return async dispatch => {
        try {
            const result = await quanLyFilmService.xoaFilm(id);
            message.success('Xóa thành công!')
            dispatch(getFilmAction())
        } catch (error) {
            console.log(error.reponse?.data)
        }
    }
}


export const layFilmAction = (id) => {
    return async dispatch => {
        try {
            const result = await quanLyFilmService.getFilm(id);
            if (result.status === 200) {
                dispatch({
                    type: SET_EDIT_FILM,
                    data: result.data.content
                })
            }
        } catch (error) {
            console.log(error.reponse?.data)
        }
    }
}
