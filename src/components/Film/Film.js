import React from 'react'
import _ from 'lodash'
import { AiOutlineHeart, AiOutlineEye } from "react-icons/ai";
import { NavLink } from 'react-router-dom';
import { Rate } from 'antd';
export default function Film(props) {
    const { film } = props;
    return (
        <div className="p-4">
            <div className="h-full bg-gray-100 bg-opacity-75 rounded-lg overflow-hidden text-center relative">
                <img src={film.hinhAnh} alt={film.tenPhim} className='w-full' style={{ height: '533px' }} />
                <h1 className="title-font sm:text-2xl text-xl font-medium text-green-700 mb-3">{_.truncate(film.tenPhim, { 'length': 30, 'separator': '' })}</h1>
                <p className="leading-relaxed mb-3 text-yellow-600 text-base px-3">{_.truncate(film.moTa, { 'length': 80, 'separator': '' })}</p>
                <div className="text-xs">
                    <button type="button" className="w-1/3 border h-12 font-bold text-base border-green-900 text-red-400 rounded-md px-4 py-2 m-2 transition duration-500 ease select-none hover:bg-green-900 hover:text-white focus:outline-none focus:shadow-outline">TRAILER</button>
                    <NavLink to={`/detail/${film.maPhim}`}>
                        <button type="button" className="w-1/3 border h-12 font-bold text-base border-green-900 text-yellow-600 rounded-md px-4 py-2 m-2 transition duration-500 ease select-none hover:bg-green-900 hover:text-white focus:outline-none focus:shadow-outline">ĐẶT VÉ</button>
                    </NavLink>

                </div>

                <div className="flex justify-between items-center px-4 mb-4 w-full">
                    <div className="flex">
                        <i className="material-icons mr-2 text-red-600 text-3xl hover:text-green-900"><AiOutlineHeart /></i>
                        <i className="material-icons text-blue-600 text-3xl hover:text-green-900"><AiOutlineEye /></i>
                    </div>
                    <div>
                        <Rate allowHalf value={film.danhGia / 2} />
                    </div>
                </div>

            </div>
        </div>
    )
}
