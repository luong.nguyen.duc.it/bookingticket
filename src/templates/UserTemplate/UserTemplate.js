
import { Route } from 'react-router';
import { useEffect } from 'react';


export const UserTemplate = (props) => {
    const { Component, ...restRoute } = props;
    useEffect(() => {
        window.scrollTo(0, 0);
    })
    return <Route {...restRoute} render={(propsRoute) => {

        return <>

            <div className="lg:flex">

                <Component {...propsRoute} />

                <div className="hidden lg:flex items-center justify-center flex-1">
                    <img className='h-screen' src='https://wallpaperaccess.com/full/1139036.jpg' alt='PHOENIX' />
                </div>
            </div>


        </>
    }} />
}