import { quanLyRapService } from "../../services/QuanLyRapService"
import { SET_LIST_RAP } from './../types/RapType';





export const getRapAction = () => {
    return async dispatch => {
        try {
            const result = await quanLyRapService.layDanhSachRap();

            if (result.status === 200) {
                dispatch({
                    type: SET_LIST_RAP,
                    lstRapChieu: result.data.content
                })
            }
        } catch (error) {
            console.log(error)
        }
    }
}