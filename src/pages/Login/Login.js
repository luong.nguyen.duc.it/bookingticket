import React from 'react'
import { NavLink } from 'react-router-dom';
import { useFormik } from 'formik'
import { useDispatch } from 'react-redux';
import { dangNhapAction } from './../../redux/actions/QuanLyNguoiDungAction';
import * as Yup from "yup";
export default function Login(props) {

    const dispatch = useDispatch();


    const formik = useFormik({
        initialValues: {
            taiKhoan: '',
            matKhau: '',
        },
        validationSchema: Yup.object({
            taiKhoan: Yup.string()
                .min(2, "Too Short!")
                .max(50, "Too Long!")
                .required("Account is required"),
            matKhau: Yup.string()
                .min(2, "Too Short!")
                .max(50, "Too Long!")
                .required("Password is required"),

        }),
        onSubmit: values => {
            dispatch(dangNhapAction(values))
        },
    });



    return (
        <div className="lg:w-1/2 xl:max-w-screen-sm">
            <div className="py-12 bg-indigo-100 lg:bg-white flex justify-center lg:justify-start lg:px-12">
                <div className="cursor-pointer flex items-center">
                    <NavLink to='/home'>
                        <img src='https://i.pinimg.com/originals/ce/a7/c1/cea7c1c3377295b4e3ba605488ea3741.png' alt='PHOENIX' className='h-36 w-36' />
                    </NavLink>
                    <div className="text-2xl text-yellow-500 tracking-wide ml-2 font-bold">PHOENIX</div>
                </div>
            </div>
            <div className="mt-10 px-12 sm:px-24 md:px-48 lg:px-12 lg:mt-16 xl:px-24 xl:max-w-2xl">
                <h2 className="text-center text-4xl text-yellow-500 font-display font-semibold lg:text-left xl:text-5xl
              xl:text-bold">Đăng nhập</h2>
                <div className="mt-12">
                    <form onSubmit={formik.handleSubmit}>
                        <div>
                            <div className="text-lg font-bold text-green-900 tracking-wide">Tài khoản</div>
                            <input name='taiKhoan' onChange={formik.handleChange} className="w-full text-lg py-2 border-b border-gray-300 focus:outline-none focus:border-yellow-500" placeholder="Nhập tài khoản..." />
                            {formik.errors.taiKhoan && formik.touched.taiKhoan && (
                                <span className='text-red-400'>{formik.errors.taiKhoan}</span>
                            )}
                        </div>
                        <div className="mt-8">
                            <div className="flex justify-between items-center">
                                <div className="text-lg font-bold text-green-900 tracking-wide">
                                    Mật khẩu
                                </div>
                                <div>
                                    <NavLink to='' className="text-xs font-display font-semibold text-yellow-500 hover:text-green-900
                                  cursor-pointer">
                                        Quên mật khẩu?
                                    </NavLink>
                                </div>
                            </div>
                            <input type='password' name='matKhau' onChange={formik.handleChange} className="w-full text-lg py-2 border-b border-gray-300 focus:outline-none focus:border-yellow-500" placeholder="Nhập mật khẩu..." />
                            {formik.errors.matKhau && formik.touched.matKhau && (
                                <span className='text-red-400'>{formik.errors.matKhau}</span>
                            )}
                        </div>
                        <div className="mt-10">
                            <button type='submit' className="text-yellow-500 text-xl font-bold border-2 border-green-900 p-4 w-full rounded-full tracking-wide
                          font-semibold font-display focus:outline-none focus:shadow-outline hover:bg-green-900 hover:text-white
                          shadow-2xl">
                                Đăng nhập
                            </button>
                        </div>
                    </form>
                    <div className="mt-12 text-sm font-display font-semibold text-green-900 text-center">
                        Bạn chưa có tài khoản ? <NavLink to='/register' className="cursor-pointer text-yellow-500 hover:text-green-900">Đăng ký</NavLink>
                    </div>
                </div>
            </div>
        </div>
    )
}
