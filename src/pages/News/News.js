import React from 'react'
import { Card } from 'antd';
import { CustomCard } from '@tsamantanis/react-glassmorphism';
import { ScheduleOutlined } from '@ant-design/icons'
const { Meta } = Card;

const renderCGV = [
    {
        hinhAnh: 'img/CGV/momo-cgv.jpg',
        lichBatDau: '27/09/2022',
        lichKetThuc: '12/12/2022'
    },
    {
        hinhAnh: 'img/CGV/avatar_cgv.png',
        lichBatDau: '23/09/2022',
        lichKetThuc: '03/10/2022'
    },
    {
        hinhAnh: 'img/CGV/Black_Adam_cgv.png',
        lichBatDau: '29/09/2022',
        lichKetThuc: '10/10/2022'
    },
    {
        hinhAnh: 'img/CGV/avatar.png',
        lichBatDau: '22/09/2022',
        lichKetThuc: '02/10/2022'
    },
    {
        hinhAnh: 'img/CGV/Birthday_Popcorn_Box_240x201.png',
        lichBatDau: '20/09/2022',
        lichKetThuc: '12/12/2022'
    },
    {
        hinhAnh: 'img/CGV/ocb_cgv.png',
        lichBatDau: '01/09/2022',
        lichKetThuc: '30/09/2022'
    },
    {
        hinhAnh: 'img/CGV/scb_cgv.png',
        lichBatDau: '10/06/2022',
        lichKetThuc: '07/10/2022'
    },
    {
        hinhAnh: 'img/CGV/shopee_cgv.jpg',
        lichBatDau: '20/09/2022',
        lichKetThuc: '31/10/2022'
    },
    {
        hinhAnh: 'img/CGV/zalo_cgv.jpg',
        lichBatDau: '01/09/2022',
        lichKetThuc: '30/09/2022'
    }
]
const renderBHD = [
    {
        hinhAnh: 'img/BHD/1920x1080-BanhKemLanh-270x152.jpg',
        title: 'MUA 1 TẶNG 1 BÁNH KEM LẠNH'
    },
    {
        hinhAnh: 'img/BHD/1920x1080-Pepsi-Back2School-270x152.jpg',
        title: 'BACK TO SCHOOL'
    },
    {
        hinhAnh: 'img/BHD/1920x1080-Popcorn-270x152.jpg',
        title: 'TÚI BẮP SẮC MÀU'
    },
    {
        hinhAnh: 'img/BHD/Suat-Khuya-Web-270x152.jpg',
        title: 'GIÁ VÉ ƯU ĐÃI CHO SUẤT KHUYA'
    },
    {
        hinhAnh: 'img/BHD/U22-web-1-270x152.png',
        title: 'ƯU ĐÃI ĐẶC BIỆT CHO U22'
    },
    {
        hinhAnh: 'img/BHD/Vani_BHD_Web-banner-1920x1080@4x-1-270x152.jpg',
        title: 'VANI APP TẶNG NGAY 40000'
    },
    {
        hinhAnh: 'img/BHD/Web-HappyDay-270x152.png',
        title: 'HAPPY MONDAY - THỨ 2 VUI VẺ'
    },
]
export default function News(props) {
    return (
        <div style={{ backgroundImage: 'url(https://img5.thuthuatphanmem.vn/uploads/2021/12/28/background-den-vang_015106698.jpg)', backgroundRepeat: 'no-repeat', backgroundSize: '100%' }}>
            <CustomCard
                effectColor="#ffff"
                blur={10}
                borderRadius={0}
            >
                <div className='mt-24 mx-96'>
                    <h1 className='text-center text-6xl font-bold text-yellow-500'>TIN MỚI VÀ ƯU ĐÃI</h1>
                    <div className='hover:scale-110 cursor-pointer w-40'>
                        <div style={{
                            width: '150px',
                            height: '40px',
                            transform: 'skew(20deg)',
                            backgroundColor: 'red',
                            position: 'absolute',
                            zIndex: -1
                        }}></div>
                        <span className='text-2xl'>CGV Cinemas</span>
                    </div>
                    <div className='grid grid-cols-4 gap-4 my-5'>
                        {renderCGV.map((item, index) => {
                            return <div key={index} className=''>
                                <img className='hover:scale-110 cursor-pointer' src={item.hinhAnh} alt='' />
                                <div className='mt-1 mb-4 flex'>
                                    <div className='text-3xl text-red-500 ml-2'><ScheduleOutlined /></div>
                                    <div className='flex items-center text-sm mx-2'>{item.lichBatDau} -</div>
                                    <div className='flex items-center text-sm'>{item.lichKetThuc}</div>
                                </div>
                            </div>
                        })}
                    </div>
                    <div className='hover:scale-110 cursor-pointer w-40 mt-28'>
                        <div style={{
                            width: '150px',
                            height: '40px',
                            transform: 'skew(20deg)',
                            backgroundColor: 'green',
                            position: 'absolute',
                            zIndex: -1
                        }}></div>
                        <span className='text-2xl m-4'>BHD Star</span>
                    </div>
                    <div className='grid grid-cols-4 gap-4 my-5'>
                        {renderBHD.map((item, index) => {
                            return <Card
                                hoverable
                                style={{ width: 240 }}
                                cover={<img className='hover:scale-110' alt="" src={item.hinhAnh} />}
                            >
                                <Meta title={item.title} />
                            </Card>
                        })}
                    </div>
                </div>

            </CustomCard>

        </div>
    )
}
