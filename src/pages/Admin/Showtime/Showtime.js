import React, { Fragment, useState, useEffect } from 'react'
import {
    InputNumber,
    DatePicker,
    Select,
} from 'antd';
import { useFormik } from 'formik'
import { quanLyRapService } from '../../../services/QuanLyRapService';
import moment from 'moment';
import { taoLichChieuAction } from '../../../redux/actions/QuanLyDatVeAction';
import { useDispatch } from 'react-redux';
export default function Showtime(props) {
    const dispatch = useDispatch();

    const [state, setState] = useState({
        heThongRapChieu: [],
        cumRapChieu: []
    })

    // eslint-disable-next-line react-hooks/exhaustive-deps
    useEffect(async () => {
        try {
            const result = await quanLyRapService.layThongTinHeThongRap();
            setState({
                ...state,
                heThongRapChieu: result.data.content
            });
        } catch (error) {
            console.log(error.reponse?.data);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])


    const formik = useFormik({
        initialValues: {
            maPhim: props.match.params.id,
            ngayChieuGioChieu: '',
            maRap: '',
            giaVe: '',
        },
        // validationSchema: Yup.object({
        //     taiKhoan: Yup.string()
        //         .min(2, "Too Short!")
        //         .max(50, "Too Long!")
        //         .required("Account is required"),
        //     matKhau: Yup.string()
        //         .min(2, "Too Short!")
        //         .max(50, "Too Long!")
        //         .required("Password is required"),

        // }),
        onSubmit: values => {
            dispatch(taoLichChieuAction(values))
        },
    });
    const optionGroupCinema = () => {
        return state.heThongRapChieu?.map((htr, index) => {
            return {
                label: htr.tenHeThongRap,
                value: htr.maHeThongRap
            }
        })
    }
    const handleChangeGroupCinema = async (value) => {

        try {
            let result = await quanLyRapService.layThongTinRap(value);
            setState({
                ...state,
                cumRapChieu: result.data.content
            })
        } catch (error) {
            console.log(error.reponse?.data);
        }
    }
    const optionCinema = () => {
        return state.cumRapChieu?.map((rap, index) => {
            return {
                label: rap.tenCumRap,
                value: rap.maCumRap
            }
        })
    }
    const handleChangeCinema = (value) => {
        formik.setFieldValue('maRap', value)
    }
    const onOk = (value) => {
        formik.setFieldValue('ngayChieuGioChieu', moment(value).format('DD/MM/YYYY hh:mm:ss'))
    }
    const handleChangeNumber = (value) => {
        formik.setFieldValue('giaVe', value)
    }
    return (
        <Fragment>
            <h2 className='text-4xl font-bold text-center text-yellow-500'>Tạo lịch chiếu</h2>
            <form onSubmit={formik.handleSubmit}>
                <div className='grid grid-cols-12 my-4'>
                    <span className='col-start-4'>Hệ thống rạp:</span>
                    <Select style={{ width: '550px' }} options={optionGroupCinema()} name='heThongRap' onChange={handleChangeGroupCinema} placeholder='Chọn hệ thống rạp...' />
                </div>
                <div className='grid grid-cols-12 my-4'>
                    <span className='col-start-4'>Rạp:</span>
                    <Select style={{ width: '550px' }} options={optionCinema()} name='rap' onChange={handleChangeCinema} placeholder='Chọn rạp...' />
                </div>
                <div className='grid grid-cols-12 my-4'>
                    <span className='col-start-4'>Ngày chiếu giờ chiếu:</span>
                    <DatePicker format='DD/MM/YYYY hh:mm:ss' showTime className='col-span-2' onOk={onOk} />
                </div>
                <div className='grid grid-cols-12 my-4'>
                    <span className='col-start-4'>Giá vé:</span>
                    <InputNumber name='giaVe' onChange={handleChangeNumber} min={70000} max={120000} />
                </div>



                <div className='text-center mt-24'>
                    <button type='submit' className="w-36 h-12 text-yellow-500 text-xl font-bold border-2 border-green-900 rounded hover:bg-green-900 hover:text-white">Thêm</button>
                </div>
            </form >
        </Fragment >
    )
}
