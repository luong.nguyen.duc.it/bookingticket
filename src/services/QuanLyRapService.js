
import { baseService } from './baseServices';
import { GROUP } from '../utils/settings/config'


export class QuanLyRapService extends baseService {

    // eslint-disable-next-line no-useless-constructor
    constructor() {
        super();
    }

    // eslint-disable-next-line no-unreachable
    layDanhSachRap = () => {
        return this.get(`/api/QuanLyRap/LayThongTinLichChieuHeThongRap?maNhom=${GROUP}`);
    }

    layThongTinHeThongRap = () => {
        return this.get(`/api/QuanLyRap/LayThongTinHeThongRap`)
    }

    layThongTinRap = (maHeThongRap) => {
        return this.get(`/api/QuanLyRap/LayThongTinCumRapTheoHeThong?maHeThongRap=${maHeThongRap}`)
    }
}

export const quanLyRapService = new QuanLyRapService();