import React from 'react'
import { createBrowserHistory } from 'history'
import { Router, Switch } from 'react-router';
import { HomeTemplate } from './templates/HomeTemplate/HomeTemplate';
import Home from './pages/Home/Home';
import Contact from './pages/Contact/Contact';
import News from './pages/News/News';
import Login from './pages/Login/Login'
import Register from './pages/Register/Register'
import Detail from './pages/Details/Detail';
import { CheckOutTemplate } from './templates/CheckOutTemplate/CheckOutTemplate';
import CheckOut from './pages/CheckOut/CheckOut';
import { UserTemplate } from './templates/UserTemplate/UserTemplate'
import Loading from './components/Loading/Loading';
import { AdminTemplate } from './templates/AdminTemplate/AdminTemplate';
import Dashboard from './pages/Admin/Dashboard/Dashboard';
import AdminFilm from './pages/Admin/Films/AdminFilm';
import AddFilm from './pages/Admin/Films/AddFilm/AddFilm';
import EditFilm from './pages/Admin/Films/EditFilm/EditFilm';

import Showtime from './pages/Admin/Showtime/Showtime';
import Profile from './pages/Profile/Profile';
import QuanLyUser from './pages/Admin/Users/QuanLyUser';
export const history = createBrowserHistory();
export default function App() {

  return (
    <Router history={history}>
      <Loading />


      <Switch>

        <HomeTemplate path="/" exact Component={Home} />
        <HomeTemplate path="/home" exact Component={Home} />
        <HomeTemplate path="/contact" exact Component={Contact} />
        <HomeTemplate path="/news" exact Component={News} />
        <HomeTemplate path="/profile" exact Component={Profile} />
        <HomeTemplate path="/detail/:id" exact Component={Detail} />

        <CheckOutTemplate path="/checkout/:id" exact Component={CheckOut} />

        <UserTemplate path="/login" exact Component={Login} />
        <UserTemplate path="/register" exact Component={Register} />

        <AdminTemplate path="/admin" exact Component={Dashboard} />
        <AdminTemplate path="/admin/films" exact Component={AdminFilm} />
        <AdminTemplate path="/admin/films/add" exact Component={AddFilm} />
        <AdminTemplate path="/admin/films/edit/:id" exact Component={EditFilm} />
        <AdminTemplate path="/admin/films/showtime/:id" exact Component={Showtime} />

        <AdminTemplate path="/admin/users" exact Component={QuanLyUser} />



      </Switch>
    </Router>
  )
}